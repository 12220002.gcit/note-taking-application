export default function CardView({title, note, order,image, onEdit, onClose, onPrev, onNext, deckName, totalCards, theme, sortType}){
  const cardViewTheme = " q-card-view-" + theme;
  const imageUrl = `data:image/jpeg;base64,${image}`;
  console.log("image",image)
  console.log("image",imageUrl)


  return (
    <div className={"d-flex flex-column vh-100 justify-content-center align-items-center q-card-view" + cardViewTheme}>
      <div>
        <div className="d-flex justify-content-center">
          <div className="text-white">
            <h4>{deckName}</h4>
          </div>
        </div>
        <div className="d-flex flex-column border border-2 q-card p-0">
          <div className="d-flex justify-content-between align-items-center">
            {/* <button className="btn" onClick={onEdit} title="Edit card"> */}
              <i className="bi bi-pen" onClick={onEdit} title="Edit card" style={{fontSize: '1.2rem'}}></i>
            {/* </button> */}
            <b>{title}</b>
            {/* <button className="btn" onClick={onClose} title="Close"> */}
              <i className="bi bi-x-lg"  onClick={onClose} title="Close" style={{fontSize: '1.2rem'}}></i>
            {/* </button> */}
          </div>
          <div className="border border-2 d-flex justify-content-between align-items-center flex-grow-1">
            <i className={"bi bi-caret-left-fill" + (onPrev ? "" : " invisible")} onClick={onPrev} title="Previous card" style={{fontSize: '1.2rem'}}></i>
            <div>{image && <img src={imageUrl} alt={title} style={{width:"150px",height:"160px", marginTop:"-280px"}}/>}{note}</div>
            <i className={"bi bi-caret-right-fill" + (onNext ? "" : " invisible")} onClick={onNext} title="Next card" style={{fontSize: '1.2rem'}}></i>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <b>{order}</b>
          </div>
        </div>
        <div className="d-flex justify-content-between">
          <div className="text-white">
            Total card: {totalCards}
          </div>
          <div className="text-white">
            Sort type: 
            {sortType === "nameAscending" || sortType === "dateAscending" || sortType === "orderAscending" ? <i className="bi bi-sort-up mx-1" /> : <i className="bi bi-sort-down me-1" />}
            {sortType === "nameAscending" || sortType === "nameDescending" ? "Name" : ""}
            {sortType === "dateAscending" || sortType === "dateAscending" ? "Date" : ""}
            {sortType === "orderAscending" || sortType === "orderDescending" ? "Order" : ""}
          </div>
        </div>
      </div>
    </div>
  );
}
