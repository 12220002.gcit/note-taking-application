import { NavLogo } from "./logo";
import { useNavigate } from "react-router-dom";
import { uri } from "../globals";


export default function SideBar({nav, title, body}){
  const navigate = useNavigate();


  
  async function signOut(){
    try{
      const res = await fetch(`${uri}/admin-sign-out`, {
        method: "DELETE",
        credentials: "include"
      });

      navigate('/');
    }
    catch(err){
      navigate('/error/' + err.message);
    }
  }

  return(
    <div className="container g-0 border">
      <div className="row g-0">
        <div className="col-12 col-lg-3">
          <div className="d-flex flex-column q-sidebar border-end">
            <div href="/" className="d-flex align-items-center py-3 link-dark text-decoration-none">
              <NavLogo />
              <h2 className="ms-2">Dashboard</h2>
            </div>
            <ul className="nav nav-pills flex-column mb-auto">
              <li>
                <div onClick={() => {navigate("/admin-statistics")}} className={"nav-link " + (nav === "statistics" ? "active" : "link-dark")}>
                  Statistics
                </div>
              </li>
              <li className="nav-item">
                <div onClick={() => {navigate("/admin-users")}} className={"nav-link " + (nav === "users" ? "active" : "link-dark")}>
                  Users
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="col-12 col-lg-9">
          <div className="shadow p-3 d-flex justify-content-between mb-5">
            <h2>{title}</h2>
            <button className="btn" title="Sign out" style={{color: 'red'}} onClick={signOut}>
              <i className="bi bi-box-arrow-right"></i>
              <span className="ms-2">
                Sign out
              </span>
            </button>
          </div>
          {body}
        </div>
      </div>
    </div>
  );
}