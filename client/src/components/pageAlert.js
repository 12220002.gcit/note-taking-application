import {NavLogo} from "../components/logo";

export default function PageAlert({type, text, loading}){
  return (
    <div className="container-fluid d-flex flex-column justify-content-center align-items-center vh-100" style={{minHeight: '400px'}}>
      <div className="d-flex align-items-center">
        <NavLogo/>
        <h1 className="ms-3">YEGU</h1>
      </div>
      {loading ?
        <div className="spinner-border text-primary" role="status"/> :
        <div className={"text-center alert alert-" + type} role="alert">
          {text}
        </div>
      }
    </div>
  );
}
