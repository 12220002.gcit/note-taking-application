
import React, {useState} from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import SignUp from "./routes/signUp";
import SignIn from "./routes/signIn";
import ForgotPassword from "./routes/forgotPassword";
import Home from "./routes/home";
import Error from "./routes/error";
import Settings from "./routes/settings";
import CreateDeck from "./routes/createDeck";
import EditDeck from "./routes/editDeck";
import Cards from "./routes/cards";
import CreateCard from "./routes/createCard";
import EditCard from "./routes/editCard";
import VerifySignUp from "./routes/verifySignUp";
import VerifyEmailChange from "./routes/verifyEmailChange";
import VerifyForgotPassword from "./routes/verifyForgotPassword";
import LandingPage from "./routes/landingPage";
import AdminSignIn from "./routes/adminSignIn";
import AdminUsers from "./routes/adminUsers";
import AdminStatistics from "./routes/adminStatistics";

import AdminContext from "./contexts/adminContext";

import './App.css';
import './css/main.min.css';

function App() {
  const [users, setUsers] = useState({});
  const [deckThemeCount, setDeckThemeCount] = useState({});
  const [cardsCount, setCardsCount] = useState(0);

  return (
    <AdminContext.Provider value={{users, setUsers, deckThemeCount, setDeckThemeCount, cardsCount, setCardsCount}}>
      <Routes>
        <Route path="/" element={<LandingPage/>} />
        <Route exact path="/sign-up" element={<SignUp/>} />
        <Route exact path="/sign-in" element={<SignIn/>} />
        <Route
          path="/admin"
          element={<Navigate to="/admin-sign-in" replace />}
        />
        <Route exact path="/admin-sign-in" element={<AdminSignIn/>} />
        <Route exact path="/admin-statistics" element={<AdminStatistics/>} />
        <Route exact path="/admin-users" element={<AdminUsers/>} />
        <Route exact path="/forgot-password" element={<ForgotPassword/>} />
        <Route exact path="/home" element={<Home/>} />
        <Route exact path="/settings" element={<Settings/>} />
        <Route exact path="/create-deck" element={<CreateDeck/>}/>
        <Route exact path="/edit-deck/:did" element={<EditDeck/>}/>
        <Route exact path="/create-card/:did" element={<CreateCard/>}/>
        <Route exact path="/cards/:did" element={<Cards/>}/>
        <Route exact path="/edit-card/:cid" element={<EditCard/>}/>
        <Route exact path="/verify-sign-up/:id/:token" element={<VerifySignUp />}/>
        <Route exact path="/verify-email-change/:id/:token" element={<VerifyEmailChange />}/>
        <Route exact path="/verify-forgot-password/:id/:token" element={<VerifyForgotPassword />}/>
        <Route exact path="/error/:message" element={<Error/>}/>
      </Routes>
    </AdminContext.Provider>
  );
}

export default App;
