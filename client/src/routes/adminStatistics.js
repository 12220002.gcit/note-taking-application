import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Chart as ChartJS } from 'chart.js/auto';
import { Chart } from 'react-chartjs-2';
import 'chartjs-adapter-date-fns';

import AdminContext from "../contexts/adminContext";
import SideBar from "../components/sideBar";

import { uri } from "../globals";

export default function AdminStatistics(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();
  const {users, setUsers, deckThemeCount, setDeckThemeCount, cardsCount, setCardsCount} = useContext(AdminContext);
  const userChartData = {}; 
  Object.values(users).forEach(data => {
    const time = data.doc.substring(0, 7);
    userChartData[time] = userChartData[time] ? userChartData[time] + 1 : 1;
  })

  let deckCount = 0;
  Object.values(deckThemeCount).forEach(count => {
    deckCount += count;
  })

  useEffect(() => {
    let ignore = false;

    fetch(`${uri}/users`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (res.ok){
        res.json().then(json => {
          const newUsers = {}
          json.forEach(user => {
            const { uid, ...others } = user;
            newUsers[uid] = others; 
          })
          setUsers(newUsers)
        })  
      } else if(!ignore && res.status === 403){
        navigate('/');
      }
    });

    fetch(`${uri}/decks-theme`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (res.ok){
        res.json().then(json => {
          const newDeckThemeCount = {};
          json.forEach(data => {
            const theme = data.theme;
            newDeckThemeCount[theme] = newDeckThemeCount[theme] ? newDeckThemeCount[theme] + 1 : 1;
          });
          setDeckThemeCount(newDeckThemeCount);
        })  
      } else if(!ignore && res.status === 403){
        navigate('/');
      }
    });
    
    fetch(`${uri}/cards-count`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (res.ok){
        res.json().then(json => {
          setCardsCount(json.count)
        })  
      } else if(!ignore && res.status === 403){
        navigate('/');
      }
    });

    return () => {ignore = true}
  }, []);

  const body=(
    <div className="row g-0">
      <div className="col-12">
        <div className="row w-100 g-0">
          <div className="col-12 col-lg-4 p-3" style={{height: 250, color: 'white'}}>
            <div className="border rounded h-100" style={{backgroundColor: '#974de7'}}>
              <h1 className="text-center mt-3">Users</h1>
              <div className="d-flex justify-content-center align-items-center">
                <span style={{fontSize: 80}}>{Object.keys(users).length}</span>
                <i className="bi bi-person-square ms-2" style={{fontSize: '4rem'}}></i>
              </div>
            </div>

          </div>
          <div className="col-12 col-lg-4 p-3" style={{height: 250, color: 'white'}}>
            <div className="border rounded h-100" style={{backgroundColor: '#974de7'}}>
              <h1 className="text-center mt-3">Decks</h1>
              <div className="d-flex justify-content-center align-items-center">
                <span style={{fontSize: 80}}>{deckCount}</span>
                <i className="bi bi-calendar3-fill ms-2" style={{fontSize: '4rem'}}></i>
              </div>
            </div>

          </div>
          <div className="col-12 col-lg-4 p-3" style={{height: 250, color: 'white'}}>
            <div className="border rounded h-100" style={{backgroundColor: '#974de7'}}>
              <h1 className="text-center mt-3">Cards</h1>
              <div className="d-flex justify-content-center align-items-center">
                <span style={{fontSize: 80}}>{cardsCount}</span>
                <i className="bi bi-back ms-2" style={{fontSize: '4rem'}}></i>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div className="col-12 col-lg-5 d-flex justify-content-center align-items-center border">
        <Chart 
          type='line' 
          data={{
            datasets: [{
              data: Object.keys(userChartData).map(date => {return {x: date, y: userChartData[date]}}),
              borderWidth: 1,
              backgroundColor: '#974de7',
              borderColor: '#974de7'
            }]
          }}
          options={{
            scales: {
              x : {
                type: 'time',
                title: {
                  display: true,
                  text: 'Sign up month'
                },
                time: {
                  unit: 'month'
                }
              },
              y : {
                title: {
                  display: true,
                  text: 'Current user count'
                },
                ticks: {
                  precision: 0
                }
              }
            },
            plugins: {
              title: {
                  display: true,
                  text: 'Users sign up date'
              },
              legend: {
                display: false
              }
            }
          }}
        >
        </Chart>
      </div>
      <div className="col-12 col-lg-7 d-flex justify-content-center align-items-center border">
        <Chart 
          type='bar' 
          data={{
            labels: ['Default', 'Salmon', 'Sky', 'Lucky', 'Banana', 'Sunset'],
            datasets: [{
              data: [deckThemeCount.default, deckThemeCount.salmon, deckThemeCount.sky, deckThemeCount.lucky, deckThemeCount.banana, deckThemeCount.sunset],
              borderWidth: 1,
              backgroundColor: '#974de7',
              borderColor: '#974de7'
            }]
          }}
          options={{
            scales: {
              x : {
                title: {
                  display: true,
                  text: 'Themes'
                }
              },
              y : {
                title: {
                  display: true,
                  text: 'Deck count'
                },
                ticks: {
                  precision: 0
                }
              }
            },
            plugins: {
              title: {
                  display: true,
                  text: 'Theme prevelance'
              },
              legend: {
                display: false
              }
            }
          }}
        >
        </Chart>
      </div>
    </div>
  );

  return (
    <SideBar nav="statistics" body={body} title="Statistics"/>
  );
}