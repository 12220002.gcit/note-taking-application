import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';
import { useParams } from "react-router-dom";
import Card from "../components/card";
import CardView from "../components/cardView";
import Empty from "../components/empty";

import { uri } from "../globals";

export default function Cards(){
  const STATUS={
    viewing : 0,
    submit : 1
  }

  const { did }= useParams();
  const navigate = useNavigate();

  const [cardSize, setCardSize] = useState(3);
  const [currentSortType, setCurrentSortType] = useState("orderAscending");
  const [search, setSearch] = useState("");
  const [currentCardId, setCurrentCardId] = useState(null);
  const [currentCardView, setCurrentCardView] = useState(null);
  const [cards, setCards] = useState([]);
  const [currentCards, setCurrentCards] = useState([]);
  const [currentDeck, setCurrentDeck] = useState(null);
  const resultModal = useRef(null);
  const isLoading = useRef(true);
  const [status, setStatus] = useState(STATUS.viewing);
  const [modalData, setModalData] = useState({title: "", body: "", delete: false});

  useEffect(() => {
    let ignore = false;
    var modal = new Modal(document.getElementById('resultModal'));
    resultModal.current = modal;

    const fetchData = async () => {
      try{
        const deckRes = await fetch(`${uri}/deck/${did}`, {
          method: "GET",
          credentials: "include"
        });
        if (deckRes.status === 403) return navigate('/home'); 

        const jsonDeck = await deckRes.json();
  
        if (ignore) return;
  
        const cardsRes = await fetch(`${uri}/cards/${did}`, {
          method: "GET",
          credentials: "include"
        });
        if (cardsRes.status === 403) return navigate('/home'); 
        const jsonCards = await cardsRes.json();
  
        jsonCards.forEach(d => {
          d.doc = new Date(d.doc);
          d.order = jsonDeck.cards.indexOf(d.cid) + 1;
        });
  
        if (ignore) return;
  
        isLoading.current = false;
        setCards(jsonCards);
        setCurrentDeck(jsonDeck);
        setCurrentCards(sortCards(jsonCards, currentSortType));
      }
      catch (err){
        return navigate("/error/" + err.message);
      }
    }

    fetchData();

    //load cookies
    fetch(`${uri}/cards-cookie`, {
        method: "GET",
        credentials: "include"
    })
    .then(res => {
      if (res.ok){
        res.json()
        .then(json => {
          if (!ignore && json) {
            setCardSize(parseInt(json.cardSize)); 
            setCurrentSortType(json.sortType);
          }
        });
      }
    });
      
    return () => {
      ignore = true;
    }
  }, []);

  async function deleteCard(){
    setStatus(STATUS.submit);
    fetch(`${uri}/card`, {
      method: "DELETE",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({cid: currentCardId, did})
    })
    .then(res => {
      setStatus(STATUS.viewing);
      if (res.ok){
        res.json()
        .then(json => {
          setModalData({title: "Success", body: <span>Card <b>{json.title}</b> has been deleted</span>, delete: false})
          setCurrentCardId("");
          setCurrentCards(currentCards.filter(card => card.cid !== json.cid));
          setCurrentDeck({...currentDeck, cards: currentDeck.cards.filter(cid => cid !== json.cid)});
        })
      } else{
        setModalData({title: "Error", body: "There was a problem when deleting the card", delete: false})
      }
    })
  }

  function sortCards(cardsToSort, sortType){
    if (sortType === "nameAscending"){
      cardsToSort.sort((a,b) => a.title.localeCompare(b.title)); 
    } else if (sortType === "nameDescending"){
      cardsToSort.sort((a,b) => b.title.localeCompare(a.title)); 
    } else if (sortType === "dateAscending"){
      cardsToSort.sort((a,b) => a.doc - b.doc); 
    } else if (sortType === "dateDescending"){
      cardsToSort.sort((a,b) => b.doc - a.doc); 
    } else if (sortType === "orderAscending"){
      cardsToSort.sort((a,b) => a.order - b.order); 
    } else if (sortType === "orderDescending"){
      cardsToSort.sort((a,b) => b.order - a.order); 
    }

    return cardsToSort;
  }

  function searchCard(event){
    event.preventDefault();
    const filteredSearch = search.trim();
    setSearch(filteredSearch);
    const re = new RegExp(`${filteredSearch}`, 'i');
    setCurrentCards(sortCards(cards.filter(card => re.test(card.title)), currentSortType));
  }

  async function updateCardSetting(size, sortType){
    setCardSize(size);
    setCurrentSortType(sortType)
    if (status === STATUS.submit){
      return;
    }

    try{
      setStatus(STATUS.submit);

      await fetch(`${uri}/cards-cookie`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({cardSize: size, sortType})
      });

      setStatus(STATUS.viewing);
    }
    catch (err){
      setStatus(STATUS.viewing);
    }
  }

  function openModal(){
    resultModal.current.show();
  }

  function closeModal(){
    resultModal.current.hide();
  }

  function SortMenuItem({text, sortType, ascending}){
    const c = "bi bi-sort-" + (ascending ? "up" : "down");

    return (
      <li><a className={"dropdown-item" + (currentSortType === sortType ? " active" : "")}
        onClick={e => {
        updateCardSetting(cardSize, sortType);
        setCurrentCards(sortCards(currentCards, sortType));
      }}>
        <i className={c}></i> {text}</a>
      </li>
    );
  }

  return (
    <div>
      <div className={currentCardView === null ? "" : "d-none"}>
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand">
              <NavLogo/>
              <span className="navbar-text d-lg-none ms-2">
                YEGU
              </span>
            </a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <div className="fw-bold me-lg-3 d-flex flex-lg-column justify-content-between align-items-center">
                <div>{currentDeck && currentDeck.name}</div>
                <div className="d-none d-lg-block">{currentDeck && currentDeck.cards.length} {currentDeck && (currentDeck.cards.length <= 1 ? "card" : "cards")}</div>
                <div className="d-lg-none ">Total card: {currentDeck && currentDeck.cards.length}</div>
              </div>
              <form className="d-flex mt-2 mb-2 mt-lg-0 mb-lg-0" onSubmit={searchCard}>
                <input className="form-control me-2" type="search" placeholder="Search cards..." aria-label="Search" value={search} 
                  onChange={e => setSearch(e.target.value)}
                />
                <button className="btn btn-outline-primary" type="submit">Search</button>
              </form>
              <div className="flex-fill" />
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <button className="btn" onClick={() => navigate('/create-card/' + did)} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add card">
                    <i className="bi bi-file-plus" style={{fontSize: '1.2rem'}}></i>
                    <span className="navbar-text d-lg-none ms-2">
                      Add card
                    </span>
                  </button>
                </li>
                <li className="nav-item">
                  <button className="btn" onClick={() => navigate('/settings')} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Account settings">
                    <i className="bi bi-gear" style={{fontSize: '1.2rem'}}></i>
                    <span className="navbar-text d-lg-none ms-2">
                      Account settings
                    </span>
                  </button>
                </li>
                <li className="nav-item dropstart">
                  <button className="btn" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" title="Card size">
                    <i className="bi bi-aspect-ratio" style={{fontSize: '1.2rem'}}></i>
                    <span className="navbar-text d-lg-none ms-2">
                      Card size
                    </span>
                  </button>
                  <ul className="dropdown-menu " aria-labelledby="navbarDropdown">
                    <li><a className={"dropdown-item" + (cardSize === 1 ? " active" : "")} onClick={e => updateCardSetting(1, currentSortType)}>
                      Very small</a>
                    </li>
                    <li><a className={"dropdown-item" + (cardSize === 2 ? " active" : "")} onClick={e => updateCardSetting(2, currentSortType)}>
                      Small</a>
                    </li>
                    <li><a className={"dropdown-item" + (cardSize === 3 ? " active" : "")} onClick={e => updateCardSetting(3, currentSortType)}>
                      Medium</a>
                    </li>
                    <li><a className={"dropdown-item" + (cardSize === 4 ? " active" : "")} onClick={e => updateCardSetting(4, currentSortType)}>
                      Large</a>
                    </li>
                    <li><a className={"dropdown-item" + (cardSize === 5 ? " active" : "")} onClick={e => updateCardSetting(5, currentSortType)}>
                      Very large</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item dropstart">
                  <button className="btn" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" title="Sort card">
                    <i className="bi bi-three-dots-vertical" style={{fontSize: '1.2rem'}}></i>
                    <span className="navbar-text d-lg-none ms-2">
                      Sort card
                    </span>
                  </button>
                  <ul className="dropdown-menu " aria-labelledby="navbarDropdown">
                    <li  className="text-center"><span>Sort ascending</span></li>
                    <li><hr className="dropdown-divider"/></li>
                    <SortMenuItem text="Order" sortType="orderAscending" ascending={true}/>
                    <SortMenuItem text="Name" sortType="nameAscending" ascending={true}/>
                    <SortMenuItem text="Date of creation" sortType="dateAscending" ascending={true}/>
                    <li><hr className="dropdown-divider"/></li>
                    <li className="text-center"><span>Sort descending</span></li>
                    <li><hr className="dropdown-divider"/></li>
                    <SortMenuItem text="Order" sortType="orderDescending" ascending={false}/>
                    <SortMenuItem text="Name" sortType="nameDescending" ascending={false}/>
                    <SortMenuItem text="Date of creation" sortType="dateDescending" ascending={false}/>
                  </ul>
                </li>
                <li className="nav-item">
                  <button className="btn w-100 mt-2 mt-lg-0 border border-2" onClick={() => navigate('/home')} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Go to decks">
                    <i className="bi bi-house" style={{fontSize: '1.2rem'}}></i>
                    <span className="navbar-text ms-2">
                      Home
                    </span>
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        {isLoading.current === true &&
        <div className="d-flex justify-content-center align-items-center w-100 vh-100 position-fixed">
          <div className="spinner-border text-primary" role="status"/>
        </div>
        }
        { currentCards.length > 0 && isLoading.current === false &&
          <div className="container-fluid mt-5 pt-5 vh-100" onClick={e => setCurrentCardId(null)}>
            <div className="row gy-2">
              {
                currentCards.map(card =>{ 
                  const deckInfo = <>
                    <div className="mb-3">Title: {card.title}</div>
                    <div>Created at: {card.doc.toDateString()}</div>
                  </>
                  const dc = <Card name={card.title} order={card.order} theme={currentDeck.theme} size={cardSize} visible={card.cid === currentCardId} 
                    onClick={e => {
                      if (currentCardId === card.cid){
                        setCurrentCardView(currentCards.findIndex(n => n.cid === card.cid));
                      } else{
                        setCurrentCardId(card.cid);
                      }
                    }}
                    onInfo={() => {
                      setModalData({title: "Card info", body: deckInfo, delete: false});
                      openModal();
                    }}
                    onEdit={() => {
                      navigate('/edit-card/' + card.cid);
                    }}
                    onDelete={() => {
                      setModalData({title: "Delete card", body: <span>Are you sure you want to delete card <b>{card.title}</b>?</span>, delete: true});
                      openModal();
                    }}
                  />

                  if (cardSize === 1){
                    return (<div key={card.cid} className="col-6 col-sm-4 col-md-3 col-lg-2 d-flex justify-content-center">
                      {dc}
                    </div>);
                  } else if (cardSize === 2){
                    return (<div key={card.cid} className="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 d-flex justify-content-center">
                      {dc}
                    </div>);
                  } else if (cardSize === 3){
                    return (<div key={card.cid} className="col-12 col-sm-6 col-lg-4 col-xl-3 d-flex justify-content-center">
                      {dc}
                    </div>);
                  } else if (cardSize === 4){
                    return (<div key={card.cid} className="col-12 col-lg-6 col-xl-4 col-xxl-3 d-flex justify-content-center">
                      {dc}
                    </div>);
                  } else if (cardSize === 5){
                    return (<div key={card.cid} className="col-12 col-lg-6 col-xxl-4 d-flex justify-content-center">
                      {dc}
                    </div>);
                  }
                })
              }
            </div>
          </div>
        }
        { currentCards.length === 0 && isLoading.current === false &&
          <div className="d-flex justify-content-center align-items-center w-100 vh-100 position-fixed">
            <Empty></Empty>
          </div>
        }

        <div className="modal" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  {modalData.title}
                </h5>
                <button type="button" className="btn-close" onClick={closeModal} disabled={status === STATUS.submit}></button>
              </div>
              <div className="modal-body">
                {modalData.body}
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" onClick={closeModal} disabled={status === STATUS.submit}>Close</button>
                {modalData.delete && <button type="button" className="btn btn-danger" onClick={deleteCard} disabled={status === STATUS.submit}>Delete</button>}
              </div>
              {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
            </div>
          </div>
        </div>
      </div>
      {currentCardView !== null &&
        <CardView {...currentCards[currentCardView]} deckName={currentDeck.name} totalCards={currentDeck.cards.length} theme={currentDeck.theme} sortType={currentSortType}
          onClose={() => setCurrentCardView(null)}
          onEdit={() => navigate('/edit-card/' + currentCards[currentCardView].cid)}
          onPrev={currentCardView === 0 ? null : () => {
            setCurrentCardView(currentCardView - 1);
          }}
          onNext={currentCardView === currentCards.length - 1 ? null : () => {
            setCurrentCardView(currentCardView + 1);
          }}
        />
      }
    </div>
  );
}