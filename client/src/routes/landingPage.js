import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";

import { uri } from "../globals";

export default function LandingPage(){
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isDownloading, setDownloading] = useState(false);

  useEffect(() => {
    let ignore = false;
    fetch(`${uri}/is-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (res.ok && !ignore) setLoggedIn(true);
    });
    return () => {ignore = true}
  }, []);

  function downloadURI(uri, name) 
  {
      var link = document.createElement("a");
      link.setAttribute('download', name);
      link.href = uri;
      document.body.appendChild(link);
      link.click();
      link.remove();
  }

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo/>
            <span className="navbar-text ms-2">
              QUICKCARDS
            </span>
          </a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            {isLoggedIn ?
              <>
                <li className="nav-item">
                  <a class="nav-link" href="/home">Home</a>
                </li>
              </> : 
              <>
                <li className="nav-item">
                  <a class="nav-link" href="/sign-in">Login</a>
                </li>
                <li className="nav-item">
                  <a class="nav-link" href="/sign-up">Sign up</a>
                </li>
              </>
            }
            </ul>
          </div>
        </div>
      </nav>

      <div className="container-fluid my-5 pt-5">
        <div className="row mx-auto" style={{maxWidth: '1000px'}}>
          <div className="col-12 col-lg-6">
            <div className="d-flex justify-content-center align-items-center">
              <div style={{width : '400px'}}>
                <img src="https://cdn-icons-png.flaticon.com/512/3813/3813681.png" className="img-fluid" alt="logo"/>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-6">
            <div className="mt-5 mt-lg-1 w-75 mx-auto">
              <h1 className="mb-3">Welcome to YEGU</h1>
              <div className="mb-5">
                The perfect tool to help you learn and retain information more effectively! 
                Our flashcard system is designed to be intuitive and easy to use, making it ideal for anyone who wants to improve their memory and learning skills.
              </div>
              <div className="mb-3">
                Try the mobile version by clicking on the download button below!
              </div>
              <button type="button" class="btn btn-primary" onClick={() => {
                setDownloading(true);
                fetch(`${uri}/app`, {
                  method: "GET",
                  credentials: "include"
                })
                .then(res => {
                  if (res.ok){
                    res.text().then(text => {
                      downloadURI(text, "Quickcards");
                    })
                  };
                });
              }} 
                disabled={isDownloading}
              >
                {isDownloading ?
                  <>
                    <span class="spinner-border spinner-border-sm me-2" role="status" aria-hidden="true"></span>
                    <span>Downloading...</span>
                  </>
                : 
                  <span>Download app</span>
                }
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}