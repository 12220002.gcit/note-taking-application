import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';

import { uri } from "../globals";


export default function CreateDeck(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();

  const [data, setData] = useState({deckName: "", theme: "default"});
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});
  const [modalData, setModalData] = useState({title: "", body: ""});
  const resultModal = useRef(null);
  
  useEffect(() => {
    let ignore = false;

    var modal = new Modal(document.getElementById('resultModal'));
    resultModal.current = modal;

    fetch(`${uri}/is-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (!ignore){
        if (!res.ok){
          navigate('/sign-in');
        }
      }
    })
    return () => {ignore = true};
  }, []);


  function openModal(){
    resultModal.current.show();
  }


  function closeModal(){
    resultModal.current.hide();
    navigate("/home");
  }


  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {...data, deckName: data.deckName.trim()}
    setData(filteredData);
    setStatus(STATUS.submit);

    try{
      const res = await fetch(`${uri}/deck`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify(filteredData)
      });

      if(!res.ok){
        if (res.status === 403){
          setModalData({title: "Failed!", body: "Failed to authorize user."});
          openModal();
        } else{
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setStatus(STATUS.typing);
            }
          });
        }
      }
      else{
        setInvalidFeedback({input: "", feedback: ""});
        setModalData({title: "Congratulations!", body: <span>You have created a new deck called <b>{data.deckName}</b></span>});
        openModal();
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }
  

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo/>
            <span className="navbar-text ms-2">
              YEGU
            </span>
          </a>
          <button className="btn" onClick={() => navigate('/home')}>
            <i className="bi bi-x-lg" style={{fontSize: '1.2rem'}}></i>
          </button>
        </div>
      </nav>
      <div className="container-fluid">
        <div className="d-flex flex-column mt-5 pt-5 justify-content-center align-items-center">
        <h3 className="mb-3">Create a new deck</h3>
        <div className="row w-100">
          <div className="col col-sm-7 mx-auto" style={{maxWidth: '450px'}}>
            <form onSubmit={onSubmit}>
              <div className="mb-3">
                <label className="form-label">Name</label>
                <input type="text" className={"form-control" + (invalidFeedback.input === "deckName" ? " is-invalid" : "")} placeholder="Enter deck name" aria-describedby="emailFeedback" autoComplete="off" value={data.deckName}
                  onChange={e => {
                    setData({...data, deckName: e.target.value});
                  }}
                />
                <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              <div className="mb-3">
                <label className="form-label">Pick a deck theme</label>
                <select className="form-select" aria-label="Default select example" defaultValue="default"
                  onChange={e => {
                    setData({...data, theme: e.target.value});
                  }}
                >
                  <option value="default">Default</option>
                  <option value="salmon">Salmon</option>
                  <option value="sky">Sky</option>
                  <option value="lucky">Lucky</option>
                  <option value="banana">Banana</option>
                  <option value="sunset">Sunset</option>
                </select>
                <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>CREATE DECK</button>
            </form>
          </div>
        </div>
        </div>
      </div>
      <div className="modal" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {modalData.title}
              </h5>
              <button type="button" className="btn-close" onClick={closeModal}></button>
            </div>
            <div className="modal-body">
              {modalData.body}
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={closeModal}>Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}