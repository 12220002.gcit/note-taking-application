import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';
import Deck from "../components/deck";
import Empty from "../components/empty";

import { uri } from "../globals";

export default function Home(){
  const STATUS={
    viewing : 0,
    submit : 1
  }

  const navigate = useNavigate();

  const [deckSize, setDeckSize] = useState(3);
  const [currentSortType, setCurrentSortType] = useState("nameAscending");
  const [status, setStatus] = useState(STATUS.viewing);
  const [search, setSearch] = useState("");
  const [currentDeckId, setCurrentDeckId] = useState(null);
  const [decks, setDecks] = useState([]);
  const [currentDecks, setCurrentDecks] = useState([]);
  const resultModal = useRef(null);
  const isLoading = useRef(true);
  const [modalData, setModalData] = useState({title: "", body: "", delete: false});

  useEffect(() => {
    let ignore = false;
    var modal = new Modal(document.getElementById('resultModal'));
    resultModal.current = modal;

    const fetchData = async () => {
      try{
        const decksRes = await fetch(`${uri}/decks`, {
          method: "GET",
          credentials: "include"
        })
        if (decksRes.status === 403) return navigate('/sign-in'); 

        const jsonDecks = await decksRes.json();
        jsonDecks.forEach(d => {
          d.doc = new Date(d.doc);
        });
  
        if (ignore) return;
  
        isLoading.current = false;
        setDecks(jsonDecks);
        setCurrentDecks(sortDecks(jsonDecks, currentSortType));
      }
      catch (err){
        navigate("/error/" + err.message);
      }
    }

    fetchData();

    //load cookies
    fetch(`${uri}/home-cookie`, {
        method: "GET",
        credentials: "include"
    })
    .then(res => {
      if (res.ok){
        res.json()
        .then(json => {
          if (!ignore && json) {
            setDeckSize(parseInt(json.deckSize)); 
            setCurrentSortType(json.sortType);
          }
        });
      }
    });

    return () => {ignore = true}
  }, []);

  async function deleteDeck(){
    setStatus(STATUS.submit);
    fetch(`${uri}/deck`, {
      method: "DELETE",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({did: currentDeckId})
    })
    .then(res => {
      setStatus(STATUS.viewing);
      if (res.ok){
        res.json()
        .then(json => {
          setModalData({title: "Success", body: <span>Deck <b>{json.name}</b> was deleted along with <b>{json.cards.length}</b> cards inside it</span>, delete: false})
          setCurrentDeckId("");
          setCurrentDecks(currentDecks.filter(deck => deck.did !== json.did));
        })
      } else{
        setModalData({title: "Error", body: "There was a problem when deleting the deck", delete: false})
      }
    })
  }

  function sortDecks(decksToSort, sortType){
    if (sortType === "nameAscending"){
      decksToSort.sort((a,b) => a.name.localeCompare(b.name)); 
    } else if (sortType === "nameDescending"){
      decksToSort.sort((a,b) => b.name.localeCompare(a.name)); 
    } else if (sortType === "dateAscending"){
      decksToSort.sort((a,b) => a.doc - b.doc); 
    } else if (sortType === "dateDescending"){
      decksToSort.sort((a,b) => b.doc - a.doc); 
    } else if (sortType === "cardsAscending"){
      decksToSort.sort((a,b) => a.cards.length - b.cards.length); 
    } else if (sortType === "cardsDescending"){
      decksToSort.sort((a,b) => b.cards.length - a.cards.length); 
    }

    return decksToSort;
  }

  function searchDeck(event){
    event.preventDefault();
    const filteredSearch = search.trim();
    setSearch(filteredSearch);
    const re = new RegExp(`${filteredSearch}`, 'i');
    setCurrentDecks(sortDecks(decks.filter(deck => re.test(deck.name)), currentSortType));
  }

  async function updateDeckSettingSetting(size, sortType){
    setDeckSize(size);
    setCurrentSortType(sortType);
    if (status === STATUS.submit){
      return;
    }

    try{
      setStatus(STATUS.submit);

      await fetch(`${uri}/home-cookie`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({deckSize: size, sortType})
      });

      setStatus(STATUS.viewing);
    }
    catch (err){
      setStatus(STATUS.viewing);
    }
  }

  function openModal(){
    resultModal.current.show();
  }

  function closeModal(){
    resultModal.current.hide();
  }

  function SortMenuItem({text, sortType, ascending}){
    const c = "bi bi-sort-" + (ascending ? "up" : "down");

    return (
      <li><a className={"dropdown-item" + (currentSortType === sortType ? " active" : "")}
        onClick={e => {
        updateDeckSettingSetting(deckSize, sortType);
        setCurrentDecks(sortDecks(currentDecks, sortType));
      }}>
        <i className={c}></i> {text}</a>
      </li>
    );
  }

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo/>
            <span className="navbar-text d-lg-none ms-2">
              YEGU
            </span>
          </a>

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <form className="d-flex mt-2 mb-2 mt-lg-0 mb-lg-0" onSubmit={searchDeck}>
              <input className="form-control me-2" type="search" placeholder="Search decks..." aria-label="Search" value={search} 
                onChange={e => setSearch(e.target.value)}
              />
              <button className="btn btn-outline-primary" type="submit">Search</button>
            </form>
            <div className="flex-fill" />
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <button className="btn" onClick={() => navigate('/create-deck')} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Add deck">
                  <i className="bi bi-plus-square" style={{fontSize: '1.2rem'}}></i>
                  <span className="navbar-text d-lg-none ms-2">
                    Add deck
                  </span>
                </button>
              </li>
              <li className="nav-item">
                <button className="btn" onClick={() => navigate('/settings')} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Account settings">
                  <i className="bi bi-gear" style={{fontSize: '1.2rem'}}></i>
                  <span className="navbar-text d-lg-none ms-2">
                    Account settings
                  </span>
                </button>
              </li>
              <li className="nav-item dropstart">
                <button className="btn" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" title="Deck size">
                  <i className="bi bi-aspect-ratio" style={{fontSize: '1.2rem'}}></i>
                  <span className="navbar-text d-lg-none ms-2">
                    Deck size
                  </span>
                </button>
                <ul className="dropdown-menu " aria-labelledby="navbarDropdown">
                  <li><a className={"dropdown-item" + (deckSize === 1 ? " active" : "")} onClick={e => updateDeckSettingSetting(1, currentSortType)}>
                    Very small</a>
                  </li>
                  <li><a className={"dropdown-item" + (deckSize === 2 ? " active" : "")} onClick={e => updateDeckSettingSetting(2, currentSortType)}>
                    Small</a>
                  </li>
                  <li><a className={"dropdown-item" + (deckSize === 3 ? " active" : "")} onClick={e => updateDeckSettingSetting(3, currentSortType)}>
                    Medium</a>
                  </li>
                  <li><a className={"dropdown-item" + (deckSize === 4 ? " active" : "")} onClick={e => updateDeckSettingSetting(4, currentSortType)}>
                    Large</a>
                  </li>
                  <li><a className={"dropdown-item" + (deckSize === 5 ? " active" : "")} onClick={e => updateDeckSettingSetting(5, currentSortType)}>
                    Very large</a>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropstart">
                <button className="btn" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" title="Sort deck">
                  <i className="bi bi-three-dots-vertical" style={{fontSize: '1.2rem'}}></i>
                  <span className="navbar-text d-lg-none ms-2">
                    Sort deck
                  </span>
                </button>
                <ul className="dropdown-menu " aria-labelledby="navbarDropdown">
                  <li  className="text-center"><span>Sort ascending</span></li>
                  <li><hr className="dropdown-divider"/></li>
                  <SortMenuItem text="Name" sortType="nameAscending" ascending={true}/>
                  <SortMenuItem text="Date of creation" sortType="dateAscending" ascending={true}/>
                  <SortMenuItem text="Total cards" sortType="cardsAscending" ascending={true}/>
                  <li><hr className="dropdown-divider"/></li>
                  <li className="text-center"><span>Sort descending</span></li>
                  <li><hr className="dropdown-divider"/></li>
                  <SortMenuItem text="Name" sortType="nameDescending" ascending={false}/>
                  <SortMenuItem text="Date of creation" sortType="dateDescending" ascending={false}/>
                  <SortMenuItem text="Total Cards" sortType="cardsDescending" ascending={false}/>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      {isLoading.current === true &&
        <div className="d-flex justify-content-center align-items-center w-100 vh-100 position-fixed">
          <div className="spinner-border text-primary" role="status"/>
        </div>
      }
      {currentDecks.length > 0 && isLoading.current === false && 
        <div className="container-fluid mt-5 pt-5 vh-100" onClick={e => setCurrentDeckId(null)}>
          <div className="row gy-2">
            {
              currentDecks.map(deck =>{ 
                const deckInfo = <>
                  <div className="mb-3">Name: {deck.name}</div>
                  <div className="mb-3">Theme: {deck.theme}</div>
                  <div className="mb-3">Total cards: {deck.cards.length}</div>
                  <div>Created at: {deck.doc.toDateString()}</div>
                </>
                const dc = <Deck name={deck.name} theme={deck.theme} size={deckSize} visible={deck.did === currentDeckId} 
                  onClick={e => {
                    if (currentDeckId === deck.did){
                      navigate("/cards/" + deck.did);
                    } else{
                      setCurrentDeckId(deck.did);
                    }
                  }}
                  onInfo={() => {
                    setModalData({title: "Deck info", body: deckInfo, delete: false});
                    openModal();
                  }}
                  onEdit={() => {
                    navigate('/edit-deck/' + deck.did);
                  }}
                  onDelete={() => {
                    setModalData({title: "Delete deck", body: <span>Are you sure you want to delete <b>{deck.name}</b> along with the <b>{deck.cards.length}</b> cards inside it?</span>, delete: true});
                    openModal();
                  }}
                />

                if (deckSize === 1){
                  return (<div key={deck.did} className="col-6 col-sm-4 col-md-3 col-lg-2 d-flex justify-content-center">
                    {dc}
                  </div>);
                } else if (deckSize === 2){
                  return (<div key={deck.did} className="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 d-flex justify-content-center">
                    {dc}
                  </div>);
                } else if (deckSize === 3){
                  return (<div key={deck.did} className="col-12 col-sm-6 col-lg-4 col-xl-3 d-flex justify-content-center">
                    {dc}
                  </div>);
                } else if (deckSize === 4){
                  return (<div key={deck.did} className="col-12 col-lg-6 col-xl-4 col-xxl-3 d-flex justify-content-center">
                    {dc}
                  </div>);
                } else if (deckSize === 5){
                  return (<div key={deck.did} className="col-12 col-lg-6 col-xxl-4 d-flex justify-content-center">
                    {dc}
                  </div>);
                }
              })
            }
          </div>
        </div> 
      }
      {currentDecks.length === 0 && isLoading.current === false && 
        <div className="d-flex justify-content-center align-items-center w-100 vh-100 position-fixed">
          <Empty></Empty>
        </div>
      }

      <div className="modal" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {modalData.title}
              </h5>
              <button type="button" className="btn-close" onClick={closeModal} disabled={status === STATUS.submit}></button>
            </div>
            <div className="modal-body">
              {modalData.body}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={closeModal} disabled={status === STATUS.submit}>Close</button>
              {modalData.delete && <button type="button" className="btn btn-danger" onClick={deleteDeck} disabled={status === STATUS.submit}>Delete</button>}
            </div>
            {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
          </div>
        </div>
      </div>
    </div>
  );
}