import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import Logo from "../components/logo";
import PageAlert from "../components/pageAlert";

import { uri } from "../globals";

export default function SignUp(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();

  const [data, setData] = useState({name: "", email: "", password: "", confirmPassword: ""});
  const [status, setStatus] = useState(STATUS.typing);
  const [emailSent, setEmailSent] = useState(false);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});

  useEffect(() => {
    let ignore = false;
    fetch(`${uri}/is-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => res.ok && !ignore && navigate('/home'));
    return () => {ignore = true};
  }, []);

  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {
      name: data.name.trim(),
      email: data.email.trim(),
      password: data.password.trim(),
      confirmPassword: data.confirmPassword.trim() 
    }
    setData(filteredData)
    setStatus(STATUS.submit);
    setInvalidFeedback({input: "", feedback: ""});
    try{
      const res = await fetch(`${uri}/sign-up-request`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(filteredData)
      });

      if(res.ok){
        setInvalidFeedback({input: "", feedback: ""});
        setEmailSent(true);
      }else if (res.status === 500){
        navigate("/error/server problem");
      }else{
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
            setStatus(STATUS.typing);
          }
        });
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }

  return (
    !emailSent ? 
    <div className="d-flex justify-content-center align-items-center" style={{minHeight: '100vh', padding: '20px'}}>
      <div className="d-flex flex-column w-100 justify-content-center align-items-center" style={{maxWidth: "420px", boxShadow: "0 0 10px", borderRadius: "10px", padding: "30px 40px", background: "rgba(255, 255, 255, 0.8)"}}>
        <a href="/"><Logo /></a>
        <h3>SIGN UP FOR YEGU</h3>
        <div className="container-fluid">
          <div className="row">
            <div className="col col-sm-12 mx-auto" style={{maxWidth: '450px'}}>
              <form onSubmit={onSubmit}>
                <div className="mb-3">
                  <label className="form-label">Name</label>
                  <input type="text" className={"form-control" + (invalidFeedback.input === "name" ? " is-invalid" : "")} placeholder="Tashi Wangchuk" aria-describedby="nameFeedback" value={data.name}
                    onChange={(e) => {
                      setData({...data, name : e.target.value});
                    }}
                  />
                  <div id="nameFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Email</label>
                  <input type="text" className={"form-control" + (invalidFeedback.input === "email" ? " is-invalid" : "")} placeholder="email@example.com" aria-describedby="emailFeedback" value={data.email}
                    onChange={(e) => {
                      setData({...data, email : e.target.value});
                    }}
                  />
                  <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input type="password" className={"form-control" + (invalidFeedback.input === "password" ? " is-invalid" : "")} aria-describedby="passwordFeedback" value={data.password} autoComplete="new-password"
                    onChange={(e) => {
                      setData({...data, password : e.target.value});
                    }}
                  />
                  <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Confirm Password</label>
                  <input type="password" className={"form-control" + (invalidFeedback.input === "confirmPassword" ? " is-invalid" : "")} aria-describedby="confirmPasswordFeedback" value={data.confirmPassword}
                    onChange={(e) => {
                      setData({...data, confirmPassword : e.target.value});
                    }}
                  />
                  <div id="confirmPasswordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
                <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>SIGN UP</button>
                <button type="button" className="btn btn-link d-block mx-auto" disabled={status === STATUS.submit} onClick={() => navigate('/sign-in')}>already have an account?</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> :
    <PageAlert type="secondary" text={<span>An email has been sent to <b>{data.email}</b></span>} loading={false}/>
  );
  
}