import React, { useEffect, useState, useRef } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import PageAlert from "../components/pageAlert";

import { uri } from "../globals";

export default function VerifyForgotPassword(){
  const navigate = useNavigate();
  const [ body, setBody ] = useState(null);
  const { id, token } = useParams();
  const alertType = useRef("danger");

  useEffect(() => {
    let timeoutId;
    const controller = new AbortController();

    const fetchData = async () => {
      try{
        const verifyRes = await fetch(`${uri}/forgot-password`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
          signal: controller.signal,
          body: JSON.stringify({id, token})
        });

        if (verifyRes.ok){
          alertType.current = "success";
          setBody("Password changed succesfully. You will be redirected to your home page in 5 seconds.");
          timeoutId = setTimeout(async () => {
            const userData = await verifyRes.json();
            const signInRes = await fetch(`${uri}/sign-in`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              credentials: "include",
              body: JSON.stringify({email: userData.email, password: userData.password})
            });
      
            if(signInRes.ok){
              navigate('/home');
            } else{
              const text= await signInRes.text();
              navigate("/error/" + text);
            }
          }, 5000);
        } else if(verifyRes.status === 500){
          const text= await verifyRes.text();
          navigate("/error/" + text);
        } else{
          const text= await verifyRes.text();
          alertType.current = "danger";
          setBody(text);

          if (verifyRes.status === 498){
            try{
              await fetch(`${uri}/forgot_password`, {
                method: "DELETE",
                headers: {
                  "Content-Type": "application/json",
                },
                credentials: "include",
                signal: controller.signal,
                body: JSON.stringify({id, token})
              });
            }
            catch(err){
              console.log(err.message);
            }
          }
        }
      }
      catch (err){
        alertType.current = "danger";
        setBody(err.message);
      }
    }

    fetchData();
  
    return () => {
      clearTimeout(timeoutId);
      controller.abort();
    };
  }, []);

  return(
    <PageAlert type={alertType.current} text={body} loading={body === null}/>
  );
}