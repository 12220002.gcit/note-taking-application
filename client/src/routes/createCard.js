import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';
import { useParams } from "react-router-dom";

import { uri } from "../globals";
import { IconContext } from 'react-icons';
import { RiUploadFill } from 'react-icons/ri';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
export default function CreateCard() {
  const STATUS = {
    typing: 0,
    submit: 1
  };
  const navigate = useNavigate();
  const { did } = useParams();

  const [data, setData] = useState({ title: "", note: "", order: 1 });
  const [currentDeck, setCurrentDeck] = useState({ deckName: "", cards: [] });
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({ input: "", feedback: "" });
  const [orderType, setOrderType] = useState("first");
  // const [modalData, setModalData] = useState({ title: "", body: "" });
  // const resultModal = useRef(null);
  const [imageFile, setImageFile] = useState(null);
  const maxOrder = useRef(1);
  const [previewUrl, setPreviewUrl] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const [statusMessage, setStatusMessage] = useState("");

  
  const closePopup = () => {
    setShowPopup(false);
    if (statusMessage==="Successfull!"){
      navigate('/home')
    }
  };
  useEffect(() => {
    let ignore = false;

    // const modalElement = document.getElementById('resultModal');
    // const modal = new Modal(modalElement);
    // resultModal.current = modal;

    fetch(`${uri}/deck/${did}`, {
      method: "GET",
      credentials: "include"
    })
      .then(res => {
        if (!ignore) {
          if (res.ok) {
            res.json()
              .then(json => {
                setCurrentDeck({ deckName: json.name, cards: json.cards });
                maxOrder.current = json.cards.length + 1;
              });
          } else if (res.status === 200) {
            navigate('/sign-in');
          } else {
            navigate('/error/' + res.text());
          }
        }
      });

    return () => { ignore = true; };
  }, [did, navigate]);

  const onSubmit = async (event) => {
    event.preventDefault();

    const filteredData = { 
        ...data, 
        title: data.title.trim(), 
        note: data.note.trim(), 
        image: imageFile 
    };
    setData(filteredData);
    setStatus(STATUS.submit);

    // Create a FormData object
    const formData = new FormData();
    formData.append('title', filteredData.title);
    formData.append('note', filteredData.note);
    formData.append('order', filteredData.order);
    formData.append('did', did);
    formData.append('image', imageFile);

    console.log("formData entries:");
    for (let pair of formData.entries()) {
        console.log(pair[0] + ': ' + pair[1]);
    }
    console.log("formDatas:", data);

    try {
        const res = await fetch(`${uri}/card`, {
            method: "POST",
            credentials: "include",
            body: formData
        });

        if (!res.ok) {
            if (res.status === 403) {
                // setModalData({ title: "Failed!", body: "Failed to authorize user." });
                setStatusMessage("Failed!")
                setShowPopup(true);            
              } else {
                const json = await res.json();
                if (json.input && json.feedback) {
                    setInvalidFeedback(json);
                    setStatus(STATUS.typing);
                }
            }
        } else {
            setInvalidFeedback({ input: "", feedback: "" });

            // setModalData({ title: "Congratulations!", body: <span>You have created a new card called <b>{data.title}</b></span> });
            setStatusMessage("Successfull!")

            setShowPopup(true);
        }
    } catch (err) {
        navigate('/error/' + err.message);
    }
  };

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo />
            <span className="navbar-text ms-2">
              YEGU
            </span>
          </a>
          <button className="btn" onClick={() => navigate('/cards/' + did)}>
            <i className="bi bi-x-lg" style={{ fontSize: '1.2rem' }}></i>
          </button>
        </div>
      </nav>
      <div className="container-fluid">
        <div className="d-flex flex-column mt-5 pt-5 justify-content-center align-items-center">
          <h3 className="mb-3">Create a new card</h3>
          <div className="row w-100">
            <div className="col col-sm-7 mx-auto" style={{ maxWidth: '450px' }}>
              <form onSubmit={onSubmit}>
                <div className="mb-3">
                  <label className="form-label">Title</label>
                  <input type="text" className={"form-control" + (invalidFeedback.input === "title" ? " is-invalid" : "")} placeholder="Enter card title" aria-describedby="emailFeedback" autoComplete="off" value={data.title}
                    onChange={e => {
                      setData({ ...data, title: e.target.value });
                    }}
                  />
                  <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="form-floating mb-3">
                  <textarea className={"form-control" + (invalidFeedback.input === "note" ? " is-invalid" : "")} placeholder="Leave a comment here" aria-describedby="noteFeedback" id="floatingTextarea2" style={{ height: '200px' }}
                    onChange={e => {
                      setData({ ...data, note: e.target.value });
                    }}
                  />
                  <label htmlFor="floatingTextarea2">Note</label>
                  {previewUrl && <img src={previewUrl} alt="Preview" style={{ width: "130px", height: "140px", marginTop: "-190px", marginLeft: "40px" }} />}
                  <div id="noteFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                  <div>
                    <label htmlFor="fileInput">
                      <button style={{ border: 'none', background: 'transparent' }} onClick={(e) => { e.preventDefault(); document.getElementById('fileInput').click(); }}>
                        <IconContext.Provider value={{ className: 'material-symbols-outlined', size: '18px' }}>
                          <RiUploadFill />
                        </IconContext.Provider>
                        <span style={{ marginLeft: '3px', marginTop: '9px', fontSize: '12px' }}>Upload Picture</span>
                      </button>
                    </label>
                    <input
                      type="file"
                      id="fileInput"
                      accept="image/*"
                      style={{ display: 'none' }}
                      onChange={e => {
                        setImageFile(e.target.files[0]);
                        setPreviewUrl(URL.createObjectURL(e.target.files[0])); // Set previewUrl to the local URL of the uploaded image
                      }} // Directly set the file
                    />
                  </div>
                  
                </div>
                <div className="mb-3">
                  <label className="form-label">Pick a card order</label>
                  <select className="form-select" aria-label="Default select example" defaultValue="first"
                    onChange={e => {
                      setOrderType(e.target.value);
                      if (e.target.value === "last") {
                        setData({ ...data, order: -1 });
                      } else {
                        setData({ ...data, order: 1 });
                      }
                    }}
                  >
                    <option value="first">First</option>
                    <option value="last">Last</option>
                    <option value="between">Between</option>
                  </select>
                  <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                {orderType === "between" &&
                  <div className="mb-3">
                    <label htmlFor="customRange3" className="form-label">Select card position</label>
                    <input type="range" className="form-range" min="1" max={maxOrder.current} step="1" id="customRange3" value={data.order}
                      onChange={e => {
                        setData({ ...data, order: e.target.value });
                      }}
                    />
                    <input type="number" className="form-control" placeholder="Enter card order" aria-describedby="emailFeedback" autoComplete="off" value={data.order}
                      onChange={e => {
                        let n = e.target.value;
                        n = Math.min(maxOrder.current, n);
                        n = Math.max(1, n);
                        setData({ ...data, order: n });
                      }}
                    />
                  </div>
                }


                <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>CREATE CARD</button>
              </form>
            </div>
          </div>
        </div>
      </div>


         {/* Popup */}
         {showPopup && (
          <div style={{
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: 1000 
          }}>
            <div style={{
              backgroundColor: ' #198754',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0 2px 5px rgba(0, 0, 0, 0.3)',
              maxWidth: '600px', 
            }}>
             
              <div>
                <p style={{
                  fontSize: '1.5em',
                  fontWeight: 'bold',
                  marginBottom: '10px'
                }}>
                  <strong>{statusMessage}</strong> {}
                </p>
               
              </div>
              <button
                onClick={closePopup}
                style={{
                  backgroundColor: '#f44336',
                  color: 'white',
                  padding: '8px 13px',
                  border: 'none',
                  borderRadius: '5px',
                }}
              >
                Ok
              </button>
            </div>
          </div>
        )}
      </div>
      
  );
}
