import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import Logo from "../components/logo";
import { uri } from "../globals";

export default function SignIn(){
  const STATUS={
    typing : 0,
    submit : 1
  }

  const navigate = useNavigate();
  const [data, setData] = useState({email: "", password: ""});
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});

  useEffect(() => {
    let ignore = false;
    fetch(`${uri}/is-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => res.ok && !ignore && navigate('/home'));
    return () => {ignore = true}
  }, []);

  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {
      email: data.email.trim(),
      password: data.password.trim()
    }
    setData(filteredData)
    setStatus(STATUS.submit);

    try{
      const res = await fetch(`${uri}/sign-in`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify(filteredData)
      });
      if(!res.ok){
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);

            setStatus(STATUS.typing);
          }
        });
      }
      else{
        setInvalidFeedback({input: "", feedback: ""});
        navigate('/home');
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }
  
  return (
    <div className="d-flex flex-column vh-100 justify-content-center align-items-center">
      <div className="d-flex flex-column w-100 justify-content-center align-items-center" style={{ maxWidth: "420px", boxShadow: "0 0 10px", borderRadius: "10px", padding: "30px 40px", background: "rgba(255, 255, 255, 0.8)" }}>
      <a href="/"><Logo /></a>
      <h3>SIGN IN</h3>
      <div className="container-fluid">
        <div className="row">
          <div className="col col-sm-7 mx-auto" style={{maxWidth: '450px', width: "100%", height: "100%"}}>
            <form onSubmit={onSubmit}>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input type="text" className={"form-control" + (invalidFeedback.input === "email" ? " is-invalid" : "")} placeholder="email@example.com" aria-describedby="emailFeedback" value={data.email}
                  onChange={(e) => {
                    setData({...data, email : e.target.value});
                  }} 
                />
                <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input type="password" className={"form-control" + (invalidFeedback.input === "password" ? " is-invalid" : "")} aria-describedby="passwordFeedback" value={data.password}
                  onChange={(e) => {
                    setData({...data, password : e.target.value});
                  }}
                />
                <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
              <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>SIGN IN</button>
              <button type="button" className="btn btn-link d-block mx-auto" disabled={status === STATUS.submit} onClick={() => navigate('/sign-up')}>don't have an account?</button>
              <button type="button" className="btn btn-link d-block mx-auto" disabled={status === STATUS.submit} onClick={() => navigate('/forgot-password')}>forgot password?</button>
            </form>
          </div>
        </div>
      </div>
      </div>
    </div>
  );
}